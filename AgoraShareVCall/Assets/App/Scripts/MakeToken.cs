using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AgoraIO.Media;

public class MakeToken : MonoBehaviour
{
    private string appId = "29fcee8b283f4507bdb1364211c2e69f";
    private string appCertificate = "61dfe60bdf24487e9e3ad6df9f3ae6ce";
    private string channelName = "Practice";
    private string uid = "0";
    private string userAccount = "User account";
    private int expirationTimeInSeconds = int.MaxValue; // The time after which the token expires
    public void testGenerateDynamicKey()
    {
        AccessToken token = new AccessToken(appId, appCertificate, channelName, uid);
        string token2 = SignalingToken.getToken(appId, appCertificate, userAccount, expirationTimeInSeconds);
        // Specify a privilege level and expire time for the token
        // token.addPrivilege(Privileges.kJoinChannel, Convert.ToUInt32(expirationTimeInSeconds));
        string result = token.build();
        
        Debug.Log(result);
    }
    // Start is called before the first frame update
    void Start()
    {
        testGenerateDynamicKey();
    }
}
