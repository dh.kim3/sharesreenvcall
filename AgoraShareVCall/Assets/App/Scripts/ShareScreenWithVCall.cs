using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Agora.Rtc;
using Agora.Util;
using TMPro;
using Sirenix.OdinInspector;
using UnityEngine.Serialization;
using Logger = Agora.Util.Logger;


public class ShareScreenWithVCall : MonoBehaviour
{
    #region SerializedVariable
    
    [SerializeField]
    private AppIdInput inputData;

    [FoldoutGroup("UIs")]
    public Button joinButton;
    [FoldoutGroup("UIs")]
    public Button leaveButton;
    [FoldoutGroup("UIs")]
    public Button startShareButton;
    [FoldoutGroup("UIs")]
    public Button stopShareButton;
    
    [FoldoutGroup("UIs")]
    public Dropdown windowSelector;

    [FoldoutGroup("UIs")]
    public Text logText;
    
    [Title("_____________Basic Configuration_____________",Bold = true, TitleAlignment =TitleAlignments.Centered),]
    [SerializeField]
    private string _appID = "";
    [SerializeField]
    private string _token = "";
    [SerializeField]
    private string _channelName = "";
    
    
    #endregion
    
    #region PrivateVariable

    private IRtcEngineEx RtcEngine = null;
    private Logger Log;

    private uint _uidCamera;
    private uint _uidScreen;

    #endregion

    #region UnityCallback

    private void Start()
    {
        LoadAssetData();
        if(CheckAppId() == false) return;
        InitEngine();
        joinButton.onClick.AddListener(JoinChannel);
    }

    #endregion

    #region PublicMethod

    

    #endregion

    #region PrivateMethod

    private bool CheckAppId()
    {
        Log = new Logger(logText);
        return Log.DebugAssert(_appID.Length > 10, "Please fill in your appId in API-Example/profile/appIdInput.asset");
    }
    private void LoadAssetData()
    {
        if (inputData == null) return;
        _appID = inputData.appID;
        _token = inputData.token;
        _channelName = inputData.channelName;
    }
    private void InitEngine()
    {
        RtcEngine = Agora.Rtc.RtcEngine.CreateAgoraRtcEngineEx();
        UserEventHandler handler = new UserEventHandler(this);
        RtcEngineContext context = new RtcEngineContext(_appID, 0,
            CHANNEL_PROFILE_TYPE.CHANNEL_PROFILE_LIVE_BROADCASTING,
            AUDIO_SCENARIO_TYPE.AUDIO_SCENARIO_DEFAULT);
        RtcEngine.Initialize(context);
        RtcEngine.InitEventHandler(new UserEventHandler(this));
    }
    private void JoinChannel()
    {
        RtcEngine.EnableAudio();
        RtcEngine.EnableVideo();
        RtcEngine.SetClientRole(CLIENT_ROLE_TYPE.CLIENT_ROLE_BROADCASTER);
            
        ChannelMediaOptions options = new ChannelMediaOptions();
        
        options.autoSubscribeAudio.SetValue(true);
        options.autoSubscribeVideo.SetValue(true);

        options.publishCameraTrack.SetValue(true);
        options.publishScreenTrack.SetValue(false);
        
        options.enableAudioRecordingOrPlayout.SetValue(true);
        
        options.clientRoleType.SetValue(CLIENT_ROLE_TYPE.CLIENT_ROLE_BROADCASTER);
        RtcEngine.JoinChannel(_token, _channelName, _uidCamera, options);
    }
    private string GetChannelName()
    {
        return _channelName;
    }

    #endregion

    #region StaticMethod

    private static void MakeVideoView(uint uid, string channelId = "", VIDEO_SOURCE_TYPE videoSourceType = VIDEO_SOURCE_TYPE.VIDEO_SOURCE_CAMERA)
    {
        var go = GameObject.Find(uid.ToString());
        if (!ReferenceEquals(go, null))
        {
            return; // reuse
        }

        // create a GameObject and assign to this new user
        VideoSurface videoSurface = new VideoSurface();

        if (videoSourceType == VIDEO_SOURCE_TYPE.VIDEO_SOURCE_CAMERA)
        {
            videoSurface = MakeImageSurface("MainCameraView");
        }
        else if (videoSourceType == VIDEO_SOURCE_TYPE.VIDEO_SOURCE_SCREEN)
        {
            videoSurface = MakeImageSurface("ScreenShareView");
        }
        else
        {
            videoSurface = MakeImageSurface(uid.ToString());
        }
        if (ReferenceEquals(videoSurface, null)) return;
        // configure videoSurface
        videoSurface.SetForUser(uid, channelId, videoSourceType);
        videoSurface.SetEnable(true);
        videoSurface.OnTextureSizeModify += (int width, int height) =>
        {
            float scale = (float)height / (float)width;
            videoSurface.transform.localScale = new Vector3(-5, 5 * scale, 1);
            Debug.Log("OnTextureSizeModify: " + width + "  " + height);
        };

    }
    private static VideoSurface MakeImageSurface(string goName)
    {
        var go = new GameObject();

        if (go == null)
        {
            return null;
        }

        go.name = goName;
        // to be renderered onto
        go.AddComponent<RawImage>();
        // make the object draggable
        go.AddComponent<UIElementDrag>();
        var canvas = GameObject.Find("VideoCanvas");
        if (canvas != null)
        {
            go.transform.parent = canvas.transform;
            Debug.Log("add video view");
        }
        else
        {
            Debug.Log("Canvas is null video view");
        }

        // set up transform
        go.transform.Rotate(0f, 0.0f, 180.0f);
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale = new Vector3(3f, 4f, 1f);

        // configure videoSurface
        var videoSurface = go.AddComponent<VideoSurface>();
        return videoSurface;
    }

    internal static void DestroyVideoView(string name)
    {
        var go = GameObject.Find(name);
        if (!ReferenceEquals(go, null))
        {
            Destroy(go);
        }
    }
    #endregion
    #region AgoraEvent

    internal class UserEventHandler : IRtcEngineEventHandler
    {
        private readonly ShareScreenWithVCall _desktopScreenShare;

        internal UserEventHandler(ShareScreenWithVCall desktopScreenShare)
        {
            _desktopScreenShare = desktopScreenShare;
        }

        public override void OnError(int err, string msg)
        {
            _desktopScreenShare.Log.UpdateLog(string.Format("OnError err: {0}, msg: {1}", err, msg));
        }

        public override void OnJoinChannelSuccess(RtcConnection connection, int elapsed)
        {
            int build = 0;
            _desktopScreenShare.Log.UpdateLog(string.Format("sdk version: ${0}",
                _desktopScreenShare.RtcEngine.GetVersion(ref build)));
            _desktopScreenShare.Log.UpdateLog(
                string.Format("OnJoinChannelSuccess channelName: {0}, uid: {1}, elapsed: {2}",
                                connection.channelId, connection.localUid, elapsed));
            if (connection.localUid == _desktopScreenShare._uidCamera)
            {
                ShareScreenWithVCall.MakeVideoView(0);
            }
            else if (connection.localUid == _desktopScreenShare._uidScreen)
            {
                ShareScreenWithVCall.MakeVideoView(0, "", VIDEO_SOURCE_TYPE.VIDEO_SOURCE_SCREEN);
            }
        }

        public override void OnRejoinChannelSuccess(RtcConnection connection, int elapsed)
        {
            _desktopScreenShare.Log.UpdateLog("OnRejoinChannelSuccess");
        }

        public override void OnLeaveChannel(RtcConnection connection, RtcStats stats)
        {
            _desktopScreenShare.Log.UpdateLog("OnLeaveChannel");
            if (connection.localUid == _desktopScreenShare._uidCamera)
            {
                ShareScreenWithVCall.DestroyVideoView("MainCameraView");
            }
            else if (connection.localUid == _desktopScreenShare._uidScreen)
            {
                ShareScreenWithVCall.DestroyVideoView("ScreenShareView");
            }
        }

        public override void OnClientRoleChanged(RtcConnection connection, CLIENT_ROLE_TYPE oldRole, CLIENT_ROLE_TYPE newRole, ClientRoleOptions newRoleOptions)
        {
            _desktopScreenShare.Log.UpdateLog("OnClientRoleChanged");
        }

        public override void OnUserJoined(RtcConnection connection, uint uid, int elapsed)
        {
            _desktopScreenShare.Log.UpdateLog(string.Format("OnUserJoined uid: ${0} elapsed: ${1}", uid, elapsed));
            if (uid != _desktopScreenShare._uidCamera && uid != _desktopScreenShare._uidScreen)
            {
                ShareScreenWithVCall.MakeVideoView(uid, _desktopScreenShare.GetChannelName(), VIDEO_SOURCE_TYPE.VIDEO_SOURCE_REMOTE);
            }
        }

        public override void OnUserOffline(RtcConnection connection, uint uid, USER_OFFLINE_REASON_TYPE reason)
        {
            _desktopScreenShare.Log.UpdateLog(string.Format("OnUserOffLine uid: ${0}, reason: ${1}", uid,
                (int)reason));
            if (uid != _desktopScreenShare._uidCamera && uid != _desktopScreenShare._uidScreen)
            {
                ShareScreenWithVCall.DestroyVideoView(uid.ToString());
            }
        }
    }

    #endregion
}
